<?php

# IMDB.com block referer, so this dirty hack gets around that!

$opts = array('http' =>
    array(
        'method'  => 'GET',
        'header'  => 'Referer: http://www.imdb.com',
    )
);

$context  = stream_context_create($opts);

header('Content-Type: image/vnd.microsoft.icon');
echo file_get_contents($_GET['image_src'], false, $context);
?>
